import 'dart:async';

class HomeBloc {
  String _urlState = "";
  final _urlStreamController = StreamController<String>();
  final _urlChangeStreamController = StreamController<String>();
  Stream<String> get urlStream => _urlStreamController.stream;
  StreamSink<String> get urlSink => _urlStreamController.sink;
  StreamSink<String> get urlChange => _urlChangeStreamController.sink;
  HomeBloc() {
    _urlStreamController.add(_urlState);
    _urlChangeStreamController.stream.listen(_changeURL);
  }
  _changeURL(String url) {
    _urlState = url;
  }

  void dispose() {
    _urlStreamController.close();
    _urlChangeStreamController.close();
  }
}
