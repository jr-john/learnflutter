import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:news_app/screens/home.dart';
import 'package:news_app/screens/news_detail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'No Bullsh*t News',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case "/detail":
            return PageTransition(
                child: NewsDetail(),
                type: PageTransitionType.fade,
                settings: settings);
            break;
          default:
            return null;
        }
      },
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.white,
        accentColor: Colors.teal,
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(title: 'No Bullsh*t News'),
    );
  }
}

/// Riverpod: State Management
/// Wrap Root App with ProviderScope. Define StateProvider. Context.read("<state>").state is the state.
///
/// Navigation:
/// 1) MaterialPageRoute
/// Navigator.push(
///   MaterialPageRoute(
///     builder:(context)=>App()
///   )
/// )
/// Too much code duplication
///
/// 2) Named Routes
/// Navigator.pushNamed("/route")
/// a - Map of routes
/// MaterialApp(
///   routes:{
///     "secondPage":(_) => SecondPage(data:"abc"),
///   }
/// )
/// Constant data passed
///
/// b - Function returning routes
/// MaterialApp(
///   onGenerateRoute: (settings){
///     switch(settings.name){
///       case:"secondPage":
///         return MaterialPageRoute(
///           builder: (_) => SecondPage(data:settings.arguments)
///         )
///     }
///   }
/// )
