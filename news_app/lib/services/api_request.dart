import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:news_app/constants/api_path.dart';
import 'package:news_app/models/news.dart';

// Parse JSON in the background
News parseNews(String responseBody) {
  var jsonNews = json.decode(responseBody);
  var news = News.fromJson(jsonNews);
  return news;
}

// Api request
Future<News> fetchNewsByCategory(String category) async {
  final response =
      await http.get("$apiURL?language=en&category=$category&apiKey=$apiKey");
  if (response.statusCode == 200)
    return compute(parseNews, response.body);
  else if (response.statusCode == 404)
    throw Exception("Not Found 404");
  else
    throw Exception("Cannot Get News");
}

// HTTP GET requests and parsing JSON in background using compute
