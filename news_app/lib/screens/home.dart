import 'package:flutter/material.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:news_app/models/news.dart';
import 'package:news_app/services/api_request.dart';
import 'package:news_app/bloc/home_bloc.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  final HomeBloc _homeBloc = HomeBloc();
  // Different news categories
  final List<Tab> tabs = <Tab>[
    new Tab(
      text: "General",
    ),
    new Tab(
      text: "Technology",
    ),
    new Tab(
      text: "Sports",
    ),
    new Tab(
      text: "Business",
    ),
    new Tab(
      text: "Entertainment",
    ),
    new Tab(
      text: "Health",
    ),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    _homeBloc.dispose();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("No Bullsh*t News"),
          bottom: TabBar(
            isScrollable: true,
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.white,
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BubbleTabIndicator(
                indicatorHeight: 25.0,
                indicatorColor: Colors.teal,
                tabBarIndicatorSize: TabBarIndicatorSize.tab),
            tabs: tabs,
            controller: _tabController,
          ),
        ),
        body: Container(
            child: StreamBuilder<String>(
                stream: _homeBloc.urlStream,
                builder:
                    (BuildContext context2, AsyncSnapshot<String> snapshot2) {
                  return TabBarView(
                    controller: _tabController,
                    children: tabs.map((tab) {
                      return FutureBuilder(
                        future: fetchNewsByCategory(tab.text),
                        builder: (context, snapshot) {
                          if (snapshot.hasError)
                            return Center(child: Text("${snapshot.error}"));
                          else if (snapshot.hasData) {
                            var newsList = snapshot.data as News;
                            var sliderList = newsList.articles != null
                                ? newsList.articles.length > 10
                                    ? newsList.articles.getRange(0, 10).toList()
                                    : newsList.articles
                                        .take(newsList.articles.length)
                                        .toList()
                                : [];
                            var contentList = newsList.articles != null
                                ? newsList.articles.length > 10
                                    ? newsList.articles
                                        .getRange(
                                            11, newsList.articles.length - 1)
                                        .toList()
                                    : []
                                : [];
                            return SafeArea(
                              child: Column(
                                children: [
                                  CarouselSlider(
                                    options: CarouselOptions(
                                        aspectRatio: 16 / 9,
                                        enlargeCenterPage: true,
                                        viewportFraction: 0.8),
                                    items: sliderList.map((item) {
                                      return Builder(
                                        builder: (context) {
                                          return GestureDetector(
                                              onTap: () {
                                                _homeBloc.urlChange
                                                    .add(item.url);
                                                Navigator.pushNamed(
                                                    context, "/detail");
                                              },
                                              child: Stack(children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  child: Image.network(
                                                      "${item.urlToImage}",
                                                      fit: BoxFit.cover),
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                        color:
                                                            Color((0xAA33639)),
                                                        child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8),
                                                            child: Text(
                                                              "${item.title}",
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            )))
                                                  ],
                                                )
                                              ]));
                                        },
                                      );
                                    }).toList(),
                                  ),
                                  Divider(
                                    thickness: 3,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8),
                                    child: Text("Trending",
                                        style: TextStyle(
                                            fontSize: 26,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  Divider(
                                    thickness: 3,
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                        itemCount: contentList.length,
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                              onTap: () {
                                                _homeBloc.urlChange.add(
                                                    contentList[index].url);
                                                Navigator.pushNamed(
                                                    context, "/detail");
                                              },
                                              child: ListTile(
                                                  leading: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8),
                                                      child: Image.network(
                                                        "${contentList[index].urlToImage}",
                                                        fit: BoxFit.cover,
                                                        height: 80,
                                                        width: 80,
                                                      )),
                                                  title: Text(
                                                    "${contentList[index].title}",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  subtitle: Text(
                                                    "${contentList[index].publishedAt}",
                                                    style: TextStyle(
                                                        fontStyle:
                                                            FontStyle.italic),
                                                  )));
                                        }),
                                  )
                                ],
                              ),
                            );
                          } else
                            return Center(child: CircularProgressIndicator());
                        },
                      );
                    }).toList(),
                  );
                })));
  }
}

/// Tabs
/// Create Tabs and TabController [SingleTickerProviderStateMixin for animations]
/// Create TabBar at bottom of Scaffold and TabBarView at body of Scaffold
///
/// API Requests - Display with FutureBuilder
/// Safe Area - Makes UI dynamic and adaptive
/// CarouselSlider, BubbleTabIndicator
/// GestureDetector - Returned by builder
/// Stack - Stack the list together
/// Expanded - Fill the space
