import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:news_app/bloc/home_bloc.dart';

class NewsDetail extends StatefulWidget {
  @override
  _NewsDetailState createState() => _NewsDetailState();
}

class _NewsDetailState extends State<NewsDetail> {
  final HomeBloc _homeBloc = HomeBloc();
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  void dispose() {
    _homeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Color(0XFFA51234)));
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: Container(
                child: StreamBuilder<String>(
                    stream: _homeBloc.urlStream,
                    builder:
                        (BuildContext context, AsyncSnapshot<String> snapshot) {
                      return SafeArea(
                          child: Container(
                              child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(children: [
                            IconButton(
                              icon: Icon(Icons.arrow_back, color: Colors.black),
                              onPressed: () => Navigator.of(context).pop(),
                              alignment: Alignment.topLeft,
                            )
                          ]),
                          Container(
                              padding: const EdgeInsets.all(10),
                              child: Container()),
                          Expanded(
                              child: WebView(
                            initialUrl: snapshot.data,
                            javascriptMode: JavascriptMode.unrestricted,
                            onWebViewCreated: (controller) {
                              _controller.complete(controller);
                            },
                          ))
                        ],
                      )));
                    }))));
  }
}

/// WebView with initialUrl and javascriptMode
/// Controller is recieved and can be used to modify and manipulate the webpage
