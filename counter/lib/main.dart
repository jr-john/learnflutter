import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // custom theme
        theme: ThemeData.dark().copyWith(
            textTheme: TextTheme(
                body1: TextStyle(
                    fontSize: 12,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                display1: TextStyle(
                    fontSize: 60,
                    color: Colors.white,
                    fontWeight: FontWeight.w900))),
        home: HomeView(title: "Counter"));
  }
}

class HomeView extends StatefulWidget {
  final String title;
  HomeView({this.title});

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  int _counter = 0;

  void _increaseCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decreaseCounter() {
    setState(() {
      _counter--;
    });
  }

  void _resetCounter() {
    setState(() {
      _counter = 0;
    });
  }

  List<Color> _color = [
    Colors.redAccent,
    Colors.blueAccent,
    Colors.greenAccent,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _color[_counter % _color.length],
      appBar: AppBar(
        title: Text(widget.title),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton(
              child: Icon(Icons.refresh),
              onPressed: _resetCounter,
            ),
            SizedBox(width: 10),
            FloatingActionButton(
              child: Icon(Icons.remove),
              onPressed: _decreaseCounter,
            ),
            SizedBox(width: 10),
            FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: _increaseCounter,
            )
          ],
        ),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Tapped button this many times",
                style: Theme.of(context).textTheme.body1,
              ),
              Text(
                "$_counter",
                style: Theme.of(context).textTheme.display1,
              )
            ]),
      ),
    );
  }
}

/// custom theme with copyWith()
/// Column with children
/// MainAxisAlignment and Center with child
/// floatingActionButton with child and onPressed
/// Stateful Widget and setState()
/// Scaffold has backgroundColor
/// Padding and floatingActionButtonLocation
