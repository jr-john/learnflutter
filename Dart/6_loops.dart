String scream(int length) => "A${'a' * length}h!";
main() {
  var numbers = [1, 2, 3];
  for (var n in numbers) print(n);
  numbers.forEach((n) => {print(n)});
  numbers.skip(1).take(1).map(scream).forEach(print);
}
