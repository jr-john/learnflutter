import 'dart:io';

int fibonacci(int n) {
  if (n <= 1) return n;
  return (fibonacci(n - 1) + fibonacci(n - 2));
}

main() {
  var input;
  int n;
  stdout.writeln("Enter n:");
  input = stdin.readLineSync();
  try {
    n = int.parse(input);
  } catch (e) {
    print("Invalid Input: Not an integer");
    exit(0);
  }
  print("nth fibonacci number: ${fibonacci(n)}");
}
