main() {
  // List
  List num = ["Jack", "Jill", 10, 100];
  // Length
  print(num.length);
  // Copying
  List<String> names = ["Jack", "Jill"];
  var names2 = names;
  names[0] = "Mark";
  for (var n in names2) print(n);
  var names3 = [...names];
  names[0] = "Milly";
  for (var n in names3) print(n);

  // Set
  var set1 = {"one", "two"};
  var set2 = {};
  var set3 = <String>{};
  Set<String> set4 = {};
  var set5 = Set();
  print(set2.runtimeType);
  print(set3.runtimeType);
  print(set4.runtimeType);
  print(set5.runtimeType);

  // map
  var map1 = {"one": 1, "two": 2};
  var maps2 = Map();
}
