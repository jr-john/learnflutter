import 'dart:io';

int binarySearch(List<num> list, int l, int r, num x) {
  if (r >= l) {
    int m = l + (r - l) ~/ 2;
    if (list[m] == x) return m;
    if (list[m] > x)
      return binarySearch(list, l, m - 1, x);
    else
      return binarySearch(list, m + 1, r, x);
  }
  return -1;
}

main() {
  var input;
  int size, error = 0;
  num x;
  // Reading size and testing if integer
  stdout.writeln("Enter size:");
  input = stdin.readLineSync();
  try {
    size = int.parse(input);
  } catch (e) {
    print("Invalid Input: Not an integer");
    exit(0);
  }
  // Reading list and testing if number
  List<num> list = [];
  for (int i = 0; i < size + error; i++) {
    input = stdin.readLineSync();
    try {
      list.add(num.parse(input));
    } catch (e) {
      print("Invalid Input: Not a number");
      error++;
    }
  }

  list.sort();

  // Reading search element and testing if number
  stdout.writeln("Enter search element:");
  input = stdin.readLineSync();
  try {
    x = int.parse(input);
  } catch (e) {
    print("Invalid Input: Not an integer");
    exit(0);
  }
  // Calling Binary Search
  int position = binarySearch(list, 0, size - 1, x);
  if (position == -1)
    print("Element not found");
  else
    print("Element found at position ${position + 1}");
}
