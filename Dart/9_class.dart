class Person {
  String name;
  int age;
  Person(this.name, [this.age = 18]);

  // Named Constructor
  Person.guest() {
    name = "Guest";
    age = 18;
  }

  void ShowOutput() {
    print(name);
    print(age);
  }

  void function() {
    print("hello");
  }
}

// class Person2 {
//   final name; // Constant but can be changed by constructor [Runtime constant]
//   static const int age =
//       12; // Class Property - Accessed by class name [Compile time constant]
// }

class Teacher extends Person {
  String subject;
  Teacher(String name, int age, this.subject) : super(name, age);

  void ShowOutput() {
    super.ShowOutput();
    print(subject);
  }

  // @override is not necessary
  @override
  void function() {
    print("world");
  }
}

class Rectangle {
  num left, top, width, height;
  Rectangle(this.left, this.top, this.width, this.height);
  // Getters and setters
  num get right => left + width;
  set right(num value) => left = value - width;
  num get bottom => top + height;
  set bottom(num value) => top = value - height;
}

// abstract classes
abstract class Describable {
  void describe();

  void describeWithEmphasis() {
    print('=========');
    describe();
    print('=========');
  }
}

main() {
  Person person1 = Person("Jerrin");
  person1.ShowOutput();
  Person person2 = Person("Jerrin", 19);
  person2.ShowOutput();
  Person person3 = Person.guest();
  person3.ShowOutput();
  Teacher person4 = Teacher("Jerrin", 18, "Maths");
  person4.ShowOutput();
}

/// The Dart compiler enforces library privacy for any identifier prefixed with an underscore.
/// Library privacy generally means that the identifier is visible only inside the file (not just the class) that the identifier is defined in.

/// implements does not inherit the functions
