main() {
  print(sum1(4, 5));
  print(sum2(num1: 4, num2: 5));
  print(sum3(4));
  print(sum4(4));
}

// Positional Parameter/Argument
dynamic sum1(var num1, var num2) => num1 + num2;
// Named Parameter/Argument
dynamic sum2({var num1, var num2}) => num1 + num2;
// Optional Parameter/Argument
dynamic sum3(var num1, {var num2 = 10}) => num1 + num2;
dynamic sum4(var num1, [var num2 = 10]) => num1 + num2;

//  Anonnymous Functions: Functions without names
