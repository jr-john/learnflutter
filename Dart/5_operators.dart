class Num {
  int num = 10;
}

main() {
  var n, m;
  int number;
  // Null Aware Operator
  number = n?.num ?? 0;
  print(number);
  print(m ??= 100);
  print(m);
}
