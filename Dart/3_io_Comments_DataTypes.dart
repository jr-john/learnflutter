import 'dart:io';

main() {
  stdout.writeln("What is your name?");
  String name = stdin.readLineSync();
  print("My name is $name");

  // Raw string
  var s = r"Raw String\n";
  print(s);

  // String Interpolation
  var age = 19;
  var string = "age = $age";
  print(string);
}

// Inline Comment

/*
Multi-line Comment
*/

/// Documentation

/*
Data Types; num, int, double, bool, String, dynamic
Static Typing & Type Inference (var)
null
Everything is object
*/
