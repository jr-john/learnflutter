int mustGreaterThanZero(int val) {
  if(val<=0)
    throw Exception("Value <= 0");
  return val;
}

void verifyValue(var val){
  var valueVerification;
  try{
    valueVerification = mustGreaterThanZero(val);
  }
  catch(e){
    print(e);
  }
  finally{
    if(valueVerification==null){
      print("Value is not valid");
    }
    else
      print("Valid value");
  }
}

main()
{
  verifyValue(10);
  verifyValue(0);
}