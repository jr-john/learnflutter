import 'dart:io';
import 'dart:math';

main() {
  // Reading Number
  var number;
  stdout.writeln("Enter number to check:");
  number = stdin.readLineSync();
  // Testing if integer
  try {
    number = int.parse(number);
  } catch (e) {
    print("Invalid Input: Not an integer");
    exit(0);
  }
  // Testing if perfect
  double sum = 1;
  for (int i = 2; i < number; i++) {
    if (number % i == 0) sum += i;
  }
  if (sum == number)
    print("Perfect");
  else
    print("Not Perfect");
}
